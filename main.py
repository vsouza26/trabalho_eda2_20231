from CoalescedHashing import CoalescedHashing
from LinearProbing import LinearProbing
from DoubleHashing import DoubleHashing

db = DoubleHashing(debug=True)
db.insert(5)
db.insert(16)
db.insert(27)
db.remove(27)
