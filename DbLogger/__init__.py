class DbLogger:
    def __init__(self, totalRegistrosReservados: int, debug = False, enableHeader : bool = False):
        self._numPulos = 0
        self._fatorDeCarga = 0
        self._carga = 0
        self._totalRegistrosReservados = totalRegistrosReservados
        self._enabled = debug 
        self._enableHeader = enableHeader

    def attFatorDeCarga(self):
        if(not self._enabled):
            return False
        self._fatorDeCarga = self._carga / self._totalRegistrosReservados 
    
    def incNumPulos(self):
        if(not self._enabled):
            return False
        self._numPulos += 1 

    def incCarga(self):
        if(not self._enabled):
            return False
        self._carga += 1 
        self.attFatorDeCarga()

    def decCarga(self):
        if(not self._enabled):
            return False
        self._carga -= 1 
        self.attFatorDeCarga()

    def terminaOperacao(self):
        if(not self._enabled):
            return False
        print(f"{self._numPulos},{self._carga},{self._fatorDeCarga}")
        self._numPulos = 0

    def incluiHeaderInsercao(self, key):
        if(not self._enabled and not self._enableHeader):
            return False
        print(f"Inserindo {key}")

    def incluiHeaderRemocao(self, key):
        if(not self._enabled and not self._enableHeader):
            return False
        print(f"Removendo {key}")

