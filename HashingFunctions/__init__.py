from math import floor
def mod_key(key: bytearray, div: int) -> int:
    return int.from_bytes(key, signed=False, byteorder='big')%div

def mod2_key(key: bytearray, div: int) ->int:
    num = int.from_bytes(key, 'big')
    if (num < div) or (num/div % div) == 0:
        return 1
    else:
        return floor(num/div)
