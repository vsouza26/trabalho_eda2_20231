from HashingFunctions import mod_key, mod2_key 
from DbLogger import DbLogger

class DoubleHashing:
    def __init__(self, min:int = 11, debug: bool =  False):
        self._regs = 0
        self._recordsReserved = min
        self._dbName = "./db.dhsh"
        self._keySize = 4
        self._sizeOfReg = self._keySize
        self._dbLogger = DbLogger(totalRegistrosReservados=min, debug=debug)
    
    def insert(self, key: str):
        '''
        INSERE INTEIRO DE 4 BITS
        '''
        if self._regs == self._recordsReserved:
            raise Exception('Numero de chaves ja excedido')
        self._dbLogger.incluiHeaderInsercao(key)
        emptyKey = b'\x00\x00\x00\x00'
        register = [0] * self._sizeOfReg 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            fd = open(self._dbName, "w+b")
        posPointer = mod_key(register, self._recordsReserved)
        bitJump = posPointer*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._keySize)
        self._dbLogger.incNumPulos()
        if reg_val == b'' or reg_val == emptyKey:
            fd.seek(bitJump,0)
            fd.write(register)
        else:
            secondHash = mod2_key(register, self._recordsReserved) 
            while True:
                posPointer = (posPointer + secondHash) % self._recordsReserved #NO MAXIMO SELF._recordsReserved - 1
                bitJump = posPointer*self._sizeOfReg
                self._dbLogger.incNumPulos()
                fd.seek(bitJump,0)
                reg_val = fd.read(self._sizeOfReg)
                if reg_val == b'' or reg_val == emptyKey:
                    fd.seek(bitJump,0)
                    fd.write(register)
                    break
        self._regs += 1
        self._dbLogger.incCarga()
        self._dbLogger.terminaOperacao()
        fd.close()

    def remove(self, key: int):
        numPulos = 0
        self._dbLogger.incluiHeaderRemocao(key)
        emptyKey = b'\x00\x00\x00\x00'
        register = [0] * self._sizeOfReg 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            return False 
        posPointer = mod_key(register, self._recordsReserved)
        bitJump = posPointer*self._sizeOfReg
        fd.seek(bitJump, 0)
        self._dbLogger.incNumPulos()
        reg_val = fd.read(self._sizeOfReg)
        if reg_val == register:
            fd.seek(bitJump,0)
            fd.write(emptyKey)
        else:
            secondHash = mod2_key(register, self._recordsReserved) 
            while True:
                posPointer = (posPointer + secondHash) % self._recordsReserved #NO MAXIMO SELF._recordsReserved - 1
                bitJump = posPointer*self._sizeOfReg
                self._dbLogger.incNumPulos()
                numPulos += 1
                fd.seek(bitJump,0)
                reg_val = fd.read(self._sizeOfReg)
                if reg_val == register:
                    fd.seek(bitJump,0)
                    fd.write(emptyKey)
                    break
                if numPulos > self._recordsReserved:
                    fd.close()
                    return False 
        self._dbLogger.decCarga()
        self._dbLogger.terminaOperacao()
        fd.close()


