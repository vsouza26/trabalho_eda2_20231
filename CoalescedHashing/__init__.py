from HashingFunctions import mod_key
from DbLogger import DbLogger

class CoalescedHashing:
    def __init__(self, min: int = 11, debug:bool = False):
        self._regs = 0
        self._recordsReserved = min
        self._dbName = "./db.chsh"
        self._keySize = 4 
        self._pointerSize = 4
        self._sizeOfReg = self._keySize + self._pointerSize 
        self._synonymList = []
        self._synonymPointer = 0
        self._dbLogger = DbLogger(totalRegistrosReservados = min, debug = debug)

    def _removeOnMem(self, key:int):
        emptyKey = b'\x00\x00\x00\x00'
        try:
            for i in range (0, len(self._synonymList[synPointer])):
                self._dbLogger.incNumPulos()
                if self._synonymList[synPointer][i] == key:
                    self._synonymList[synPointer][i] = emptyKey
                    return
        except:
            return

    def remove(self, key: int):
        '''
        REMOVE A CHAVE NA BASE DE DADOS. GUARDA INT EM QUATRO BYTE 
        '''
        self._dbLogger.incluiHeaderRemocao(key)
        emptyKey = b'\x00\x00\x00\x00'
        register = [0] * self._keySize 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            fd = open(self._dbName, "w+b")
        bitJump = mod_key(register[0:self._keySize], self._recordsReserved)*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._sizeOfReg)
        self._dbLogger.incNumPulos()
        if reg_val[0:self._keySize] == register:
            fd.seek(bitJump, 0)
            fd.write(emptyKey)
        else:
            self._removeOnMem(key)
        self._dbLogger.decCarga()
        self._dbLogger.terminaOperacao()
        fd.close()


    def _insertOnMem(self, register :bytearray(), synPointer :int):
        '''
        INSERE O REGISTRO EM UMA LISTA PRIVADA 
        '''
        emptyKey = b'\x00\x00\x00\x00'
        try:
            for i in range (0, len(self._synonymList[synPointer])):
                self._dbLogger.incNumPulos()
                if self._synonymList[synPointer][i] == emptyKey:
                    self._synonymList[synPointer][i] = register
                    return
            self._synonymList[synPointer].append(register)
        except:
            self._synonymList.append([register])

    def insert(self,key : int):
        '''
        INSERE A STRING NA BASE DE DADOS. GUARDA INT EM QUATRO BYTE 
        '''
        if self._regs == self._recordsReserved:
            raise Exception('Numero de chaves ja excedido')
        emptyKey = b'\x00\x00\x00\x00'
        self._dbLogger.incluiHeaderInsercao(key)
        register = [0] * self._sizeOfReg 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        #Adiciona ponteiro no final do registro
        for i in range (self._keySize,self._sizeOfReg):
            register[i] = 255
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            fd = open(self._dbName, "w+b")
        bitJump = mod_key(register[0:self._keySize], self._recordsReserved)*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._sizeOfReg)
        self._dbLogger.incNumPulos()
        if reg_val[0:self._keySize] == b'' or reg_val[0:self._keySize] == emptyKey:
            fd.seek(bitJump, 0)
            fd.write(register)
        else:
            synonymPointer = reg_val[self._keySize:self._sizeOfReg] 
            if synonymPointer == b'\xFF\xFF\xFF\xFF':
                fd.seek(bitJump + self._keySize, 0)
                fd.write(self._synonymPointer.to_bytes(self._pointerSize, 'big'))
                synonymPointer = self._synonymPointer.to_bytes(self._pointerSize, 'big')
                self._synonymPointer += 1
            self._insertOnMem(register[0:self._keySize], int.from_bytes(synonymPointer, 'big'))
        self._dbLogger.incCarga()
        self._dbLogger.terminaOperacao()
        self._regs += 1
        fd.close()
         
    def memDump(self):
        try:
            for i in self._synonymList:
                print("synList: ")
                for j in i:
                    print(j)
                print("\n")
        except Exception as e:
            print(f"Houve um erro {e}") 

    def _findRecusive(self, key:bytearray, pos:int, it:int):
        try:
            if self._synonymList[pos][it] == key:
                return True
            else:
                _findRecusive(key, pos, it+1)
        except:
            return False

    def find(self, key :str):
        '''PROCURA CHAVE EM BASE DE DADOS'''
        if len(key) > 16:
            raise Exception('Chave maior que o permitido')
        searchableKey = bytearray(key.encode("ascii"))
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            raise Exception('Base de dados nao criada')
        bitJump = mod_key(searchableKey, self._recordsReserved)*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._keySize)
        croppedText = bytearray()
        for i in reg_val:
            if i != 0:
                croppedText.append(i)
        offset = fd.read(4)
        offset = int.from_bytes(offset, 'big')
        if croppedText == searchableKey:
            return True
        else:
            return self._findRecusive(searchableKey, offset, 0)



    

