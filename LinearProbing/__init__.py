from HashingFunctions import mod_key
from DbLogger import DbLogger

class LinearProbing:
    def __init__(self, min:int = 11, debug: bool = False):
        self._regs = 0
        self._recordsReserved = min
        self._dbName = "./db.lhsh"
        self._keySize = 4
        self._sizeOfReg = self._keySize
        self._dbLogger = DbLogger(totalRegistrosReservados = min, debug=debug)

    def insert(self, key: str):
        '''
        INSERE INTEIRO DE 4 BITS
        '''
        if self._regs == self._recordsReserved:
            raise Exception('Numero de chaves ja excedido')
        emptyKey = b'\x00\x00\x00\x00'
        self._dbLogger.incluiHeaderInsercao(key)
        register = [0] * self._sizeOfReg 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            fd = open(self._dbName, "w+b")
        bitJump = mod_key(register[0:self._keySize], self._recordsReserved)*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._sizeOfReg)
        self._dbLogger.incNumPulos()
        if reg_val == b'' or reg_val == emptyKey:
            fd.seek(bitJump,0)
            fd.write(register)
        else:
            test = self._keySize * (self._recordsReserved)
            while True:
                pointer = fd.tell()
                reg_val_aux = fd.read(self._keySize)
                self._dbLogger.incNumPulos()
                if reg_val_aux == b'' or reg_val_aux == emptyKey:
                    fd.seek(pointer, 0)
                    fd.write(register)
                    break
                if pointer == test:
                    fd.seek(0,0)
        self._regs += 1
        self._dbLogger.incCarga()
        self._dbLogger.terminaOperacao()
        fd.close()

    def remove(self,key:int):
        numPulos = 0
        emptyKey = b'\x00\x00\x00\x00'
        self._dbLogger.incluiHeaderRemocao(key)
        register = [0] * self._sizeOfReg 
        register = bytearray(register)
        number_byte = bytearray(key.to_bytes(self._keySize, 'big'))
        for i in range(0,self._keySize):
            register[i] = number_byte[i] 
        try:
            fd = open(self._dbName, "r+b")
        except FileNotFoundError:
            return False 
        bitJump = mod_key(register, self._recordsReserved)*self._sizeOfReg
        fd.seek(bitJump, 0)
        reg_val = fd.read(self._sizeOfReg)
        self._dbLogger.incNumPulos()
        if reg_val == register:
            fd.seek(bitJump, 0)
            fd.write(emptyKey)
        else:
            test = self._keySize * (self._recordsReserved)
            while True:
                if numPulos == self._recordsReserved:
                    break
                pointer = fd.tell()
                reg_val_aux = fd.read(self._keySize)
                self._dbLogger.incNumPulos()
                numPulos += 1
                if reg_val_aux == key:
                    fd.seek(pointer, 0)
                    fd.write(emptyKey)
                    break
                if pointer == test:
                    fd.seek(0,0)
        fd.close()
        self._dbLogger.terminaOperacao()
